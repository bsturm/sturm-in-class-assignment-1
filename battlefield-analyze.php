<!DOCTYPE html>
<head>
<title>Battlefield Analysis</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
        font:12px/16px Verdana, sans-serif; /* default font */
       
}
 h1 {
        font-size:36pt;
        text-align:center;
        }
 h2 {
        font-size:20pt;
        text-align:center;
        }
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body><div id="main">
<h1>Battlefield Analysis</h1>
<h2>Latest Critiques</h2>
<h2>Battle Statistics</h2>




<?php


    $host="localhost"; // Host name 
    $username="wustl_inst"; // Mysql username 
    $password="wustl_pass"; // Mysql password 
    $db_name="battlefield"; // Database name 
    $tbl_name="reports"; // Table name 

    // Connect to server and select databse.
    $mysqli = new mysqli('ec2-54-227-50-129.compute-1.amazonaws.com', 'TA', 'cse330rocks', 'battlefield');
    if($mysqli->connect_errno){
        printf("Connection Failed: %s\n", $mysqli->connect_error);
        exit;
    }


    
    
    $stmt = $mysqli->prepare("select critique from reports order by posted desc limit 5");
    if(!$stmt) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->execute();
    $stmt->bind_result($critique);
    while ($stmt->fetch()){
       echo ("<ul> <li> $critique </li> </ul>");
       echo '<br></br>'; 
    }
    
    
    $comment = $mysqli->prepare("select soldiers, ammunition/duration as 'pounds of ammunition per second' from reports group by soldiers order by soldiers desc");

	if(!$comment) {
        printf("Query prep failed: %s\n", $mysqli->error);
        exit;
    }
    $comment->execute();
    $comment->bind_result($soldiers, $avg_ammo);
    
    while ($comment->fetch()){
       echo ("<table border='1'><tr><th>Number Of Soldiers</th><th>pounds of ammunition per second</th></tr>
             <tr><td>$soldiers</td><td>$avg_ammo</td></tr></table>");
       echo '<br></br>'; 
    }

    echo('<a href="http://ec2-54-200-21-53.us-west-2.compute.amazonaws.com/~bsturm/battlefield-submit.html">Submit a New Battle Report</a>');


?>



</div></body>
</html>






